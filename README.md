# retryer

ES6 Retry promise. Retries the given function until it succeeds given a number 
of retries and an interval between them. They are set by default to retry 5 
times with 1sec in between. There's also a flag to make the cool-down time
exponential.

based on https://gist.github.com/briancavalier/842626

Can be used like so:
```javascript
var date = new Date();
// resolve when more than 2sec from start date
async function twoSecondsPassed(){
    const curr = new Date() - date;
    if (curr > 2000) return `yay! ${curr}`;
    else throw `${curr} < 2000`;
}

(async () => {
  // retry 5 times with exponential backoff (100ms, 200ms, 400ms, 800ms, 1.6s, err)
  const data = await retry(twoSecondsPassed, 5, 100, true);
  // retry infinite times with 10 inbetween (10ms, 10ms, 10ms, ...)
  // const data = await retry(twoSecondsPassed, -1, 10);
  // retry infinite times with 10 inbetween with exponential backoff (10ms, 20ms, 40ms, ...)
  // const data = await retry(twoSecondsPassed, -1, 10, true);
  // retry 5 times with a second inbetween (1s, 1s, 1s, 1s, 1s, err)
  // const data = await retry(twoSecondsPassed);
  console.log(data);
})();
```